# pamVmTester
### Automated data extractor for CyberArk.

The goal of this script is to provide a list of the elements present on CyberArk through a combination of scraping and OCR.

## Getting started
### Installing required components
To be able to run this script, Python, PIP, Google Chrome and Tesseract are required.

Tesseract build I used for Windows : https://github.com/UB-Mannheim/tesseract/wiki

Development/testing environment is `Windows 10` / `Python 3.10.4` / `PIP 22.1.2` / `Google Chrome 103.0` / `Tesseract 5.2.0`

To install Python, just get the latest executable, make sure to select `install pip` during setup.

Google Chrome version is not critical, and Python/PIP/Tesseract MUST all be in the path.

To install the required python libs via PIP, please run `pip install -r ./requirements.txt`

Before running, make sure that your display settings are as follow :
<kbd>![Display settings: 100% scaling at 1080p](https://gitlab.com/BoeufWorks-LN/pamvmtester/-/raw/dev/DOC/WIN_SCALING.PNG)</kbd>

### Then you need to prepare your browser:
1. Open your browser, here I used `Google Chrome` due to company policy, you can also obviously use `Chromium`
2. Open the login page for your `CyberArk` instance
3. Login to your `CyberArk` instance manually
4. Load your page of interest on `CyberArk`, you can do a search if you wish to filter.
5. Once the search results are loaded, minimize your browser and resize it in height so that only one result can be seen and most (95% not more!) of the width of the screen is occupied by the window, as follow:
<kbd>![Sorry, you need to see to do this...](https://gitlab.com/BoeufWorks-LN/pamvmtester/-/raw/dev/DOC/WIN_RESIZE.PNG)</kbd>

## How to run
Follow instructions from getting started then run `python ./main.py`

When the execution finishes, you will be able to find the data in a CSV file named with the timestamp of the program's launch.

By default the CSV and LOG files are located in the same directory as main.py

## Authors and acknowledgment
Code by Louis NAILI, sample testing list partially provided by Yann and Fabricio.

## License
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/License_icon-bsd.svg/256px-License_icon-bsd.svg.png?20100126054010" width="64"/>
BSD Zero Clause License

## Project status
Currently under development, contact me if any issue arises.
