'''
    PAM VM TESTER
    LOUIS NAILI 07/2022
    HTTPS://GITLAB.COM/BOEUFWORKS-LN/PAMVMTESTER
    PYTHON 3
'''
# Libraries
# All of these libraries can be automatically installed using "pip install ./requirements.txt"
import time # Used for setting delays and monitoring time
import numpy as nm # Used to manipulate arrays for images
import pytesseract # Used for OCR
import cv2 # Used to apply color filters to images
from PIL import ImageGrab # Used for capturing images from the screen
import pyautogui # Used to interact with keyboard and mouse "as a user"
import pyperclip # Used to get data from the clipboard
import keyboard # Used for catching keystroke to exit program
import os # Used for exiting program cleanly and cleaning console
import logging # Used for making proper log files
from tqdm import tqdm # Used for progress bars



# Global variables
# These variables should be treated like settings, change them only if you know what you are doing
TIMEOUT = 60 # Timeout in seconds, after this delay, the VM will be considered as unavailable
# All provided positions are in absolute coordinates
PAMLISTITMPOS = (230, 330) # Position for current item in the list on the PAM
CRTBOXOKPOS = (950, 710) # Position of the 'YES' button on the certificate msg box
TIMBOXOKPOS = (500, 900) # Position of the 'YES' button on the timeout msg box
PAMCONNECTBTN0POS = (1800,300) # Position of the first connect button on the PAM
PAMCONNECTBTN1POS = (1140,680) # Position of the second connect button on the PAM
PAMHTML5BTNPOS = (840,620) # Position of the HTML checkbox on the PAM
VMNAMEBOXBOUNDS = (650,220,1500,260) # Bounds of the VM name on the PAM
PAMNBVMSBOUNDS = (100,240,200,240) # Bounds of the number of VMs on the PAM
MSGBOXBOUNDS = (730, 437, 1208, 732) # Bounds of the common msg box to analyze in Guacamole
PAMCONNECTEDINDPOS = (1540 ,870 ,1890 ,940) # Bounds of the "You are being recorded" message in Guacamole
PAMCONNEXIONCHECK0 = (0,270,350,550) # Bounds of the welcome message in an SSH session in Guacamole
PAMCONNEXIONCHECK1 = (0,70,350,160) # Bounds of the welcome message in an SSH session in Guacamole
PAMCONNEXIONCHECK2 = (0,90,192,124) # Bounds of the welcome message in an SSH session in Guacamole
RDPBOXBOUNDS = (1195,75,1215,95) # Bounds of the "x" in the title bar of a RDP session in Guacamole
RDPCREDCHECK = (500,300,1300,900) # Bounds of the "Your Credentials are invalid" in RDP session in Guacamole
MSGBOXRDPBOUNDS = (750,520,1230,590) # Bounds of the "Connection to ***" in RDP session in Guacamole
MSGBOXERRBOUNDS = (0,70,420,250) # Bounds of the "Host does not exist" in SSH session in Guacamole
MSGTOBOXBOUNDS = (880,560,1085,580) # Bounds of the "Timed out" message in SSH session in Guacamole
MSGCONNECTERRORBOUNDS = (750,527,1145,562) # Bounds of the "Connection error" in RDP session in Guacamole
SECONDTABNAMEBOUNDS = (490,5,530,40) # Bounds of the name of the second tab
PAMKEYINVALIDBOUNDS = (0,70,350,220)
MOTD = "Warning : This program is provided without any warranty.\nBy running this program you acknowledge that YOU are taking responsibility over the program's actions.\nTo exit the program, you can press 'escape' at any time"


# GLOBAL CONFIGURATION
# All global config is done here
TIME = str(time.time()) # Get init time

pytesseract.pytesseract.tesseract_cmd = "tesseract" # Set tesseract path to executable

clearConsole = lambda: os.system('cls' if os.name in ('nt', 'dos') else 'clear') # ClearConsole for unix and windows

# Logger configuration
logging.basicConfig(filename=TIME+".log", format='%(asctime)s %(message)s', filemode='w')
logger=logging.getLogger()
logger.setLevel(logging.DEBUG)



# CALLBACKS
# Test every pressed key to check if esc key has been pressed and exit if so, this would make for an great keylogger :p
def exitCallback(event):
    if("esc" in event.name):
        os._exit(130) # Use same exit code as ctrl+c termination

keyboard.on_release(callback=exitCallback) # When a key is pressed, send the key to exitCallback

def writeCSV(line):
    csvFile = open(TIME+'.csv', 'a')
    csvFile.write(line+'\n')
    csvFile.close()


# DATA EXTRACTION
# Returns OCR text within given bounding box
def boxToText(x0,y0,x1,y1,flag):
    cap = ImageGrab.grab(bbox =(x0, y0, x1, y1)) # Grab an image of the selected part of the screen
    # Config setting
    conf = ""
    if(flag==1):
        conf="--psm 7"
    # Actual OCR
    text = pytesseract.image_to_string(
           cv2.cvtColor(nm.array(cap), cv2.COLOR_BGR2GRAY),
           lang ='eng',
           config=conf)
    return text

# Returns copy/pasteable text within provided bounds
def getTextBetween(x0,y0,x1,y1):
    pyautogui.click(x0, y0)
    pyautogui.dragTo(x1, y1, 0.4, button='left')
    pyautogui.hotkey('ctrl', 'c')
    return pyperclip.paste()

# Returns the selected VM's name when on PAM
def getVmName():
    time.sleep(0.25)
    name = getTextBetween(*VMNAMEBOXBOUNDS)
    return name.replace('\r\n', '')



# HID EMULATION
# Click at a given position
def clickAt(x,y):
    pyautogui.click(x, y)

# Minimize current window
def minimizeWindow():
    pyautogui.hotkey('win', 'down')

# Maximize current window
def maximizeWindow():
    pyautogui.hotkey('win', 'up')

# Close current tab
def closeTab():
    pyautogui.hotkey('ctrl', 'w')

# Checks if there is more than one tab
def isSecondTabOpen():
    return True if("+" in boxToText(*SECONDTABNAMEBOUNDS,1)) else False



# COORDINATION
# Returns the status for current VM
def getStatus():
    # Wait for PAM "you are being recorded" message before attempting anything
    while not ("recorded" in boxToText(*(PAMCONNECTEDINDPOS),0)):
        {}

    time.sleep(1) # Wait for msgBox to appear, in case of slow PAM

    # Get current msgBox message for RDP
    msg = boxToText(*(MSGBOXRDPBOUNDS),1)
    logger.info(msg) #LOGGING

    # Check if server is being accessed through Windows RDP
    if("Connecting to" in msg):
        # Initialize time for TimeOut evaluation
        initTO = time.time()
        while ((initTO+TIMEOUT)>=time.time()):
            if ("x" in boxToText(*(RDPBOXBOUNDS),1)):
                return "OK"
            elif ("Your credentials" in boxToText(*(RDPCREDCHECK),0)):
                return "Authentication failure"
            elif ("(Code: 3)" in boxToText(*(MSGCONNECTERRORBOUNDS),0)):
                return "Connection error"
            elif ("(Code:" in boxToText(*(MSGBOXBOUNDS),0)):
                return "Connection error"
        return("Timed Out")

    # If we're here then the connection is done through SSH

    # Get current msgBox message for SSH
    msg = boxToText(*(MSGBOXBOUNDS),0)
    logger.info(msg)#LOGGING

    # Accept SSH certificate if not already cached
    if("nat cached in the registry." in msg):
        clickAt(*CRTBOXOKPOS)
        time.sleep(2) # Wait for message to appear after accepting certificate
        msg = boxToText(*(MSGBOXBOUNDS),0)
        logger.info(msg)#LOGGING

    # Case where connection is SSH and message indicates authentication failure
    if("Authentication failure" in msg):
        return "Authentication failure"

    # Case where connection is SSH and message indicates host not found PAM
    if("computer cauld not be found," in msg):
        return "Host not found"

    check = boxToText(*(PAMKEYINVALIDBOUNDS),0)
    if("refused our key" in check):
        return "SSH key invalid"

    # Case where connection is SSH and message indicates host not found SSH tool
    check = boxToText(*(MSGBOXERRBOUNDS),0)
    logger.info(check)#LOGGING
    if("Host does nat exist" in check):
        return "Host not found"

    # Case where connection is SSH and message indicated failure of the ssh client
    if("PSMSSHClient initialization failed," in check):
            return "SSH client failure"

    time.sleep(1) # Wait for message to appear / used for slow PAM

    # Case where connection is SSH and check is valid 1st checkpoint
    check = boxToText(*(PAMCONNEXIONCHECK0),0)
    if(len(check)>0):
        return "OK"

    # Case where connection is SSH and check is valid 2nd checkpoint
    check = boxToText(*(PAMCONNEXIONCHECK1),0)
    if("username" in check):
        check = boxToText(*(PAMCONNEXIONCHECK2),0)
        if("refused" in check):
            return "Authentication failure"
        return "OK"

    if("refused our key" in check):
        return "SSH key invalid"

    # Initialize time for TimeOut evaluation
    initTO = time.time()
    # Case where nothing was found, hence no window was found, wait until Timeout
    while((initTO+TIMEOUT)>=time.time()):
        # Check for self reported Timeout
        msg = boxToText(*(MSGTOBOXBOUNDS),0)
        logger.info(msg)#LOGGING
        if("Connection timed out" in msg):
            return("Timed Out")

    # If nothing is displayed and not self reported as timed out, declare as timed out to avoid program hang
    return("Timed Out / Unknown error")


# UI
# Format printed text for progress bar
def printD(message):
    print(">>> " + message)


# SEQUENCER
def main():
    writeCSV('"VM NAME";"STATUS";"TIME TAKEN"')
    print(MOTD)
    input("Press Enter to begin ...")
    nbVMs = int(getTextBetween(*PAMNBVMSBOUNDS).split(" ")[0])
    clearConsole()
    printD("Found "+str(nbVMs)+" VMs to test")
    for vm in tqdm(range(nbVMs)) :
        initTime = time.time()
        clickAt(*PAMLISTITMPOS)
        time.sleep(1) # Required for window refresh, cannot be OCR'd
        # Once window refresh has occured, wait for VM name to appear
        while not (len(boxToText(*VMNAMEBOXBOUNDS,1))>0):
            {}
        vmName = getVmName()
        logger.info(vmName)#LOGGING
        maximizeWindow()
        time.sleep(1) # Required for window refresh, cannot be OCR'd
        clickAt(*PAMCONNECTBTN0POS)
        time.sleep(1.5) # Required for window refresh, cannot be OCR'd
        clickAt(*PAMHTML5BTNPOS)
        time.sleep(0.5) # Required for window refresh, cannot be OCR'd
        clickAt(*PAMCONNECTBTN1POS)
        status=getStatus()
        clearConsole()
        timeTaken = time.time() - initTime
        printD("VM n."+str(vm+1)+" -> "+status)
        writeCSV('"'+vmName+'";"'+status+'";"'+str(timeTaken)+'"')
        if(isSecondTabOpen()):
            closeTab()
        minimizeWindow()
        clickAt(*PAMLISTITMPOS)
        pyautogui.press('down')
    clearConsole()
    print("Finished testing all of the "+str(nbVMs)+" VMs")

main()
